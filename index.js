console.log ("Pokemon!")




let trainer1={
	Name: "Ash Ketchup",
	Age: 16,
	Pokemon:["Bulbasur","Squirtle","Charizard","Pikachu"],
	Friends:["Misty,Brock,Elm,Jessie,James"],
	talk:function(){
		console.log(" Pikachu! I choose you!")
	}
};

console.log("result of dot notation:");
console.log (trainer1.Name);
console.log("result of Bracket notation:");
console.log (trainer1.Pokemon);
trainer1.talk();


function Pokemon(Name, Level, Health, Attack){
	let thisPokemon=trainer1.Pokemon;

	this.Name=Name;
	this.Level=Level;
	this.Health=Level*3;
	this.Attack=Level*1.5;

	this.tackle=function(pokemonName, pokemonAttack){
		console.log(pokemonName+"tackled"+ this.Name);
		let currentHp=this.Health - pokemonAttack;
		if(currentHp<=0){
			console.log(this.Name+ "hp is reduced to "+ currentHp);
			console.log(this.faint());
		}else{
			console.log(this.Name+"'s health is now reduced to "+ currentHp);
		}
	};
	this.faint=function(){
		console.log(this.Name+ " has fainted!")
	}

}

let pokemon1 = new Pokemon("Pikachu",12,36,18);
let pokemon2 =new Pokemon("Geodude",8,24,12);
let pokemon3 = new Pokemon("Mewto",100,300,150);

console.log(pokemon1);
console.log(pokemon2);
console.log(pokemon3);


pokemon1.tackle("Geodude",12);

console.log (pokemon1);

pokemon2.tackle("Mewto",150);

console.log(pokemon2);
